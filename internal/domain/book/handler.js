import {
  createBook,
  deleteBook,
  findBook,
  findBookById,
  updateBook,
} from './usecase.js';
import {
  InputNotValidException,
  NotFoundException,
} from '../../errors/errors.js';

const createErrorMessage = (message, status = 'fail') => {
  const response = {
    status: status,
    message: message,
  };
  return response;
};

const createSuccessResponse = (data, message = '') => {
  const response = {
    status: 'success',
    message,
    data,
  };
  if (message == '') delete response['message'];
  if (Object.keys(data).length == 0) delete response['data'];
  return response;
};
export const create = (request, _) => {
  const {payload} = request;
  // TODO: implement me
  try {
    const result = createBook(payload);
    return _.response(
        createSuccessResponse(result, 'Buku berhasil ditambahkan'),
    ).code(201);
  } catch (err) {
    if (err instanceof InputNotValidException) {
      return _.response(createErrorMessage(err.message)).code(
          err.statusCode,
      );
    }
    return _.response(
        createErrorMessage('Buku gagal ditambahkan', 'error'),
    ).code(500);
  }
};

export const find = (request, _) => {
  const params = request.query;
  try {
    const result = findBook(params);
    const response = {
      books: result,
    };
    return _.response(createSuccessResponse(response)).code(200);
  } catch (err) {
    return _.response(
        createErrorMessage('Gagal menampilkan data buku', err.stack),
    ).code(500);
  }
};

export const update = (request, _) => {
  const {payload} = request;
  const {bookId} = request.params;
  // TODO: implement me
  try {
    updateBook(bookId, payload);
    return _.response(
        createSuccessResponse({}, 'Buku berhasil diperbarui'),
    ).code(200);
  } catch (err) {
    if (err instanceof InputNotValidException) {
      return _.response(createErrorMessage(err.message)).code(
          err.statusCode,
      );
    }
    if (err instanceof NotFoundException) {
      return _.response(createErrorMessage(err.message)).code(
          err.statusCode,
      );
    }
    return _.response(
        createErrorMessage('Buku gagal diperbarui', 'error'),
    ).code(500);
  }
};

export const findById = (request, _) => {
  const {bookId} = request.params;
  try {
    const result = findBookById(bookId);
    return _.response(createSuccessResponse({book: result})).code(200);
  } catch (err) {
    if (err instanceof NotFoundException) {
      return _.response(createErrorMessage(err.message)).code(
          err.statusCode,
      );
    }
    return _.response(
        createErrorMessage('Gagal mencari data buku', 'error'),
    ).code(500);
  }
};

export const destroy = (request, _) => {
  const {bookId} = request.params;
  try {
    deleteBook(bookId);
    return _.response(
        createSuccessResponse({}, 'Buku berhasil dihapus'),
    ).code(200);
  } catch (err) {
    if (err instanceof InputNotValidException) {
      return _.response(createErrorMessage(err.message)).code(
          err.statusCode,
      );
    }
    if (err instanceof NotFoundException) {
      return _.response(createErrorMessage(err.message)).code(
          err.statusCode,
      );
    }
    return _.response(
        createErrorMessage('Buku gagal dihapus', 'error'),
    ).code(500);
  }
};
