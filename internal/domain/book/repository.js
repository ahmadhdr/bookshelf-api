import model from './model.js';
import {nanoid} from 'nanoid';
export const create = (request) => {
  const now = new Date().toISOString();
  const {pageCount, readPage} = request;
  const payload = {
    id: nanoid(),
    ...request,
    finished: pageCount === readPage,
    insertedAt: now,
    updatedAt: now,
  };
  model.push(payload);
  return {
    bookId: payload.id,
  };
};
export const find = (params) => {
  let response = model;
  for (const key in params) {
    if (Object.prototype.hasOwnProperty.call(params, key)) {
      if (['reading', 'finished'].includes(key)) {
        let value = false;
        if (params[key] == 1) value = true;
        response = response.filter((item) => item[key] == value);
      }
      if (['name'].includes(key)) {
        response = response.filter((item) =>
          item[key].toLowerCase().includes(params[key].toLowerCase()),
        );
      }
    }
  }

  return response;
};

export const findById = (bookId) => {
  return model.find((item) => item.id == bookId);
};

export const update = (bookId, payload) => {
  const index = model.findIndex((item) => item.id == bookId);
  model[index] = payload;
  return model[index];
};

export const destroy = (bookId) => {
  const index = model.findIndex((item) => item.id == bookId);
  model.splice(index, 1);
  return model;
};
