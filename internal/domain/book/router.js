import {create, find, findById, update, destroy} from './handler.js';

const routes = [
  {
    method: 'POST',
    path: '/books',
    handler: create,
  },
  {
    method: 'GET',
    path: '/books',
    handler: find,
  },
  {
    method: 'GET',
    path: '/books/{bookId}',
    handler: findById,
  },
  {
    method: 'PUT',
    path: '/books/{bookId}',
    handler: update,
  },
  {
    method: 'DELETE',
    path: '/books/{bookId}',
    handler: destroy,
  },
];

export default routes;
