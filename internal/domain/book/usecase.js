import {
  InputNotValidException,
  NotFoundException,
} from '../../errors/errors.js';
import {create, find, findById, update, destroy} from './repository.js';

export const createBook = (request) => {
  const {name, readPage, pageCount} = request;
  if (name == '' || name == undefined) {
    throw new InputNotValidException(
        'Gagal menambahkan buku. Mohon isi nama buku',
    );
  }
  if (readPage > pageCount) {
    throw new InputNotValidException(
        'Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount',
    );
  }
  return create(request);
};

export const findBook = (params) => {
  const response = find(params);
  return response.map((item) => {
    return {
      id: item.id,
      name: item.name,
      publisher: item.publisher,
    };
  });
};

export const findBookById = (bookId) => {
  const result = findById(bookId);
  if (result == undefined) throw new NotFoundException('Buku tidak ditemukan');
  return result;
};

export const updateBook = (bookId, request) => {
  const {name, readPage, pageCount} = request;
  const result = findById(bookId);
  if (result == undefined) {
    throw new NotFoundException(
        'Gagal memperbarui buku. Id tidak ditemukan',
    );
  }
  if (name == '' || name == undefined) {
    throw new InputNotValidException(
        'Gagal memperbarui buku. Mohon isi nama buku',
    );
  }
  if (readPage > pageCount) {
    throw new InputNotValidException(
        'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount',
    );
  }
  for (const key in request) {
    if (request.hasOwnProperty(key)) {
      result[key] = request[key];
    }
  }
  return update(bookId, result);
};

export const deleteBook = (bookId) => {
  const result = findById(bookId);
  if (result == undefined) {
    throw new NotFoundException('Buku gagal dihapus. Id tidak ditemukan');
  }
  const response = destroy(bookId);
  return response;
};
