import hapi from '@hapi/hapi';
import bookRouter from '../internal/domain/book/router.js';

const PORT = 5000;
const HOST = 'localhost';
const routes = [...bookRouter];
const config = {
  port: PORT,
  host: HOST,
};
const server = hapi.server({
  ...config,
  routes: {
    cors: {
      origin: ['*'], // an array of origins or 'ignore'
      headers: ['Authorization'], // an array of strings - 'Access-Control-Allow-Headers'
      exposedHeaders: ['Accept'], // an array of exposed headers - 'Access-Control-Expose-Headers',
      additionalExposedHeaders: ['Accept'], // an array of additional exposed headers
      maxAge: 60,
      credentials: true, // boolean - 'Access-Control-Allow-Credentials'
    },
  },
});
server.route(routes);
server.start();
console.log(`Server running at port ${PORT}`);
